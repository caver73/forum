const cookie = require('cookie');
const sessionConf = require('config').get('session');
const signature = require('cookie-signature');
const {User} = require('../model').models;
const {messageService} = require('../service');

module.exports = function (server, store) {
    const io = require('socket.io')(server);

    io.use((socket, next) => {
        let reqCookie = socket.request.headers.cookie;
        let parsedCookie = cookie.parse(reqCookie);
        let sid = parsedCookie[sessionConf.key];
        let sessionId;
        if (sid && sid.substr(0, 2) === 's:') {
            sessionId = signature.unsign(sid.slice(2), sessionConf.secret);
        }
        if (sessionId) {
            store.get(sessionId, async (err, sess) => {
                if (sess && sess.passport.user) {
                    let user = await User.findByPk(sess.passport.user);
                    if(user){
                        socket.handshake.user = user;
                    }
                }
                next();

            });
        }
    });

    io.on('connection', socket => {
        socket.on('new message', async msg => {
            let user = socket.handshake.user;
            if(user) {
                let mess = JSON.parse(msg);
                let result = await messageService.addMessage(mess, user.id);
                io.emit('added message', result);
            }
        });

        socket.on('delete message', async messageId =>{
            await messageService.deleteMessage(messageId);
            io.emit('deleted message', messageId);
        });
    });
};