const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const bodyParser = require('body-parser');
const passport = require('./security');
const session = require('express-session');
const config = require('config');
const fs = require('fs');

const indexRouter = require('./routes/index');

const sessionConf = JSON.parse(JSON.stringify(config.get('session')));

const app = express();

app.set('view engine', 'pug');
app.set('views', './views');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: true }));

sessionConf.store = new session.MemoryStore();
app.use(session(sessionConf));
app.use(passport.initialize());
app.use(passport.session());

app.use('/', (req, res, next)=>{
    if(req.user){
        res.locals.user = req.user;
    }
    next();
});
app.use('/', indexRouter);
initRouters();

app.use('/logout', (req, res)=>{
  req.logout();
  res.redirect('/');
});

// catch 404 and forward to error handler
app.use((req, res, next)=> next(createError(404)));

// error handler
app.use((err, req, res, next)=> {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

checkWorkFolders();

function initRouters() {
    fs.readdirSync('./routes').forEach(route=>{
      if(route.split('.').pop() === 'js'){
        app.use('/'+route.slice(0, -3), require('./routes/'+route.slice(0, -3)));
      }
    });
}

function checkWorkFolders(){
    Object.values(config.get('workFolders')).forEach(folder=>{
        let wf = path.join(__dirname, folder);
        fs.stat(wf, (err, stat)=>{
            if(err && err.code === 'ENOENT'){
                fs.mkdirSync(wf, {recursive:true});
            }
        });
    });
}

module.exports = app;

module.exports.initSocket = function(server) {
    require('./socket')(server, sessionConf.store);
};
