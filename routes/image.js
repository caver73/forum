const router = require('express').Router();
const fs = require('fs');
const path = require('path');
const multer = require('multer');
const {avatars} = require('config').get('workFolders');
const uploadFolder = path.join(__dirname, '..', avatars);
const upload = getUpload(uploadFolder);

router.get('/avatar/:userId', async (req, res, next) => {
    res.sendFile(path.join(uploadFolder, req.params.userId + '.jpg'));
});

router.post('/upload', upload.single('userImg'), (req, res, next) => {
    res.redirect('/profile');
});

function getUpload(folder) {
    let storage = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, folder);
        },
        filename: function (req, file, cb) {
            cb(null, req.user.id + file.originalname.slice(file.originalname.lastIndexOf('.')));
        }
    });

    return multer({storage: storage});
}

module.exports = router;