const express = require('express');
const router = express.Router();
const path = require('path');
const {Theme} = require('../model').models;

/* GET home page. */
router.get('/', async (req, res, next)=> {
  let themes = await Theme.findAll({include: ['creator']});
  console.log(JSON.stringify(themes));
  res.render('index', {themes: themes});
});

module.exports = router;
