const router = require('express').Router();
const fs = require('fs');
const path = require('path');
const multer = require('multer');
const libFolder = path.join(__dirname, '..', require('config').get('workFolders').library);
const upload = getUpload(libFolder);
const {Library, User} = require('../model').models;

router.get('/', async (req, res, next) => {
    let lib = await Library.findAll({include:['author']});
    res.render('library', {items: lib});
});

router.get('/:publicationId/:fileName', async (req, res, next) => {
    res.sendfile(path.join(libFolder, String(req.params.publicationId), req.params.fileName));
});

router.post('/', upload.single('attachment'), (req, res, next) => {
    if(!req.user) {
        res.locals.error='not authorized';
        next();
    }
    res.redirect('/library');
});

function getUpload(folder){
    let storage = multer.diskStorage({
        destination:  async (req, file, cb)=> {
            if(!req.user) {
                return cb('not authorized');
            }
            let lib = await Library.create(req.body);
            await lib.setAuthor(await User.findByPk(req.user.id));
            await lib.update({fileName: file.originalname});
            if(lib.id){
                let upFolder = path.join(folder, String(lib.id));
                fs.mkdir(upFolder, (err) => {
                    cb(null, upFolder);
                });
            }
        },
        filename: (req, file, cb)=> {
                cb(null, file.originalname);
        }
    });

    return multer({ storage: storage });
}

module.exports = router;