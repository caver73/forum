const express = require('express');
const router = express.Router();
const path = require('path');
const {Theme, User} = require('../model').models;

//gets a list of themes
router.get('/', async (req, res, next)=> {
    let themes = await Theme.findAll();
    res.json(themes);
});

//creates a theme
router.post('/', async (req, res, next)=> {
    if(!req.user){
        next();
    }
    let theme = await Theme.create(req.body);
    await theme.setCreator(await User.findByPk(req.user.id));
    res.redirect(`/message?theme=${theme.id}`);
});

//creates a new theme
router.put('/', async (req, res, next)=> {
    let theme = await Theme.create(req.body);
    await theme.setCreator(await User.findByPk(req.user));
    res.redirect('/message');
});

router.delete('/:id', async (req, res, next)=> {
    let theme = await Theme.findByPk(req.params.id);
    await theme.destroy();
    res.end();
});

module.exports = router;