const router = require('express').Router();


router.get('/', async (req, res, next) => {
    if(req.user){
        res.render('profile');
    } else {
       next();
    }
});

module.exports = router;