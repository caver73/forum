const express = require('express');
const router = express.Router();
const path = require('path');
const {Message, Theme, User} = require('../model').models;
const {messageService} = require('../service');

//gets a list of messages for a theme
router.get('/', async (req, res, next) => {
    let themeId = req.query.theme;
    let messages = await messageService.getMessages(themeId);
    let theme = await Theme.findByPk(themeId);
    res.render('message', {messages: messages, theme: theme});
});

//updates message
router.post('/', async (req, res, next) => {
    let data = req.body;
    let message = await Message.findByPk(data.id);
    await message.update(data);
    res.end();
});

//creates a new message in a message.
router.put('/', async (req, res, next) => {
    let result = await messageService.addMessage(req.body, req.user.id);
    res.json(result);
});

// deletes a message from a message
router.delete('/:id', async (req, res, next) => {
    await messageService.deleteMessage(req.params.id);
    res.end();
});

module.exports = router;