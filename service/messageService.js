const {Message, Theme, User} = require('../model').models;

exports.addMessage = async (data, userId) => {
    let message = await Message.create(data);
    let author = await User.findByPk(userId);
    let theme = await Theme.findByPk(data.themeId);
    await message.setAuthor(author);
    await message.setTheme(theme);
    return Message.findByPk(message.id, {include: 'author'});
};

exports.getMessages = async themeId => {
    return Message.findAll({
        where: {themeId: themeId},
        include: ['author'],
        order: [['updatedAt', 'DESC']]
    });
};

exports.deleteMessage = async messageId => {
    let message = await Message.findByPk(messageId);
    return message.destroy();
};