const {sequelize} = require('./model');
sequelize.authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    })
    .then(()=>{
        sequelize.sync({force: true});
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });