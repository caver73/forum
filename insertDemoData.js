const {User, Theme} = require('./model').models;

createDemo();

async function createDemo() {
    await Theme.destroy({where: {}});
    await User.destroy({where: {}});
    await User.bulkCreate([
        {login: 'jack', password: '123'},
        {login: 'paul', password: '123'},
        {login: 'john', password: '123'}

    ]);
    console.log('Demo users have been created.');
}