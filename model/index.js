const config = require('config');
const dbConfig = config.get('db');
const path = require('path');
const Sequelize = require('sequelize');
const fs = require('fs');
const basename = path.basename(module.filename);
const sequelize = new Sequelize(dbConfig.dbName, dbConfig.user, dbConfig.password, dbConfig);
const models = {};

fs
    .readdirSync(__dirname)
    .filter(file=>file!==basename && file.slice(-3)==='.js')
    .forEach(file=>{
        let model = require(path.resolve(__dirname, file));
        model.init(sequelize, Sequelize.DataTypes);
        models[model.name] = model;
    });

Object.keys(models).forEach(modelName=>models[modelName].associate(models));

module.exports.models = models;
module.exports.sequelize = sequelize;