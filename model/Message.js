const Sequelize = require('sequelize');

module.exports = class Message extends Sequelize.Model {
    static init(sequelize, DataTypes) {
        super.init({
                text: Sequelize.STRING
            },
            {sequelize,
                modelName: 'Message'});
    }

    static associate(model){
        this.theme = this.belongsTo(model.Theme, {onDelete: 'cascade'});
        this.author = this.belongsTo(model.User, {as: 'author'});
    }
};