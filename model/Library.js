const Sequelize = require('sequelize');

module.exports = class Library extends Sequelize.Model {
    static init(sequelize, DataTypes){
        super.init({
            name: DataTypes.STRING,
            description: DataTypes.STRING,
            fileName: DataTypes.STRING

        },
            {
                sequelize,
                modelName: 'Library'
            })
    }

    static associate(model){
        this.author = this.belongsTo(model.User, {as: 'author'});
    }
};