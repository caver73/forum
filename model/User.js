const Sequelize = require('sequelize');

module.exports = class User extends Sequelize.Model {
    static init(sequelize, DataTypes) {
        super.init({
            login: {type: Sequelize.STRING, unique: true, allowNull: false},
            password: Sequelize.STRING
        }, {
            sequelize,
            modelName: 'User'
        });
    }
    static associate(models) {}
};