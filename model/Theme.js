const Sequelize = require('sequelize');

module.exports = class Theme extends Sequelize.Model {
    static init(sequelize, DataTypes) {
        super.init({
            name: Sequelize.STRING
        },
            {sequelize,
            modelName: 'Theme'});
    }
    static associate(models) {
        this.messages = this.hasMany(models.Message, {
            onDelete: 'cascade'
        });
        this.creator = this.belongsTo(models.User, {as: 'creator'});
    }
};