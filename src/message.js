import io from 'socket.io-client';
const socket = io();



$('form').submit(e =>{
    e.preventDefault();
    let form = e.currentTarget;
    socket.emit('new message', JSON.stringify({themeId: form.theme.value, text: form.newmsg.value}));
    return false;
});

socket.on('added message', msg =>{
    console.log(msg);
    addNewMessage(msg);
});

socket.on('deleted message', msgId =>{
    $('#messid'+msgId).remove();
});

$('[msgid]').click(deleteMessage);

function sendNewMessage(e) {
    $.ajax('/message', {
        type: "PUT",
        data: {
            themeId: $('#theme').val(),
            text: $('#newmsg').val()
        },
        success: addNewMessage
    });
}

function deleteMessage(e) {
    $.ajax(`/message/${$(e.target).attr('msgid')}`, {
        type: 'DELETE',
        success: () => $(e.target).parents('li').remove()
    });
    socket.emit('delete message', $(e.target).attr('msgid'));
}

function addNewMessage(data) {
    console.log(data);
    $("#messages").prepend($('<li>')
        .append($('<div>')
            .addClass('msg-head')
            .append($('<div>')
                .addClass('author')
                .text(data.author.login))
            .append($('<div>')
                .addClass('time')
                .text(data.updatedAt)))
        .append($('<div>')
            .addClass('msg-body')
            .text(data.text))
        .append($('<div>')
            .addClass('msg-footer')
            .append($(`<input>`, {
                type: "button",
                value: "Удалить",
                msgid: data.id,
                click: deleteMessage
            })))
    );

    $('#newmsg').val("");
}