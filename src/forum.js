$("#create_btn").click(createTheme);
$("#new_theme_cancel").click(cancelNewTheme);


function createTheme() {
    $("#create_btn").css("display", "none");
    $("#new_theme").css("display", "");
}

function cancelNewTheme() {
    $("#create_btn").css("display", "");
    $("#new_theme").css("display", "none");
}