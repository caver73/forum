const passport = require('passport');
const {Strategy} = require('passport-local');
const {User} = require('../model').models;

passport.use(new Strategy({usernameField: 'login'},
    async(username, password, cb)=>{
    try {
        let user = await User.findOne({where: {login: username}});
        if(user && user.password===password) cb(null, user);
        else cb(null, false);
    } catch (e) {
        cb(e);
    }

}));

passport.serializeUser((user, cb)=>cb(null, user.id));

passport.deserializeUser(async(id, cb)=>{
    try {
        let user = await User.findByPk(id);
        cb(null, user);
    } catch (e) {
        cb(e);
    }
});

module.exports = passport;