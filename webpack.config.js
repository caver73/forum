const path = require('path');
const webpack = require('webpack');

module.exports = {
    context: path.resolve(__dirname, './src'),
    entry: {
        themes: './forum.js',
        message: './message.js'
    },
    output: {
        path: path.resolve(__dirname, './public/scripts'),
        filename: '[name].bundle.js',
    },
    watch: false,
    watchOptions: {
        aggregateTimeout: 100
    },
    devtool: 'inline-source-map',
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: [/node_modules/],
                use: [
                    {
                        loader: 'babel-loader',
                        options: { presets: ['es2015', 'es2016', 'stage-0'] }
                    }
                ]
            }
        ]
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
        })
    ]
};